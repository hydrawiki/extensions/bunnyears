<?php
/**
 * Curse Inc.
 * BunnyEars
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		BunnyEars
 * @link		https://gitlab.com/hydrawiki
 *
**/

if ( function_exists( 'wfLoadExtension' ) ) {
 	wfLoadExtension( 'BunnyEars' );
 	wfWarn(
 		'Deprecated PHP entry point used for BunnyEars extension. Please use wfLoadExtension instead, ' .
 		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
 	);
 	return;
 } else {
 	die( 'This version of the BunnyEars extension requires MediaWiki 1.25+' );
 }
