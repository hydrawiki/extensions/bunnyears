(function(window, $, mw) {
	"use strict";
	var handleBunnyEars = function(config, onComplete){
		mw.log('Bunny Ears handler fired with options: ', config);
		$.getScript(config.stationsServiceURL + '/be/bunnyears-1.3.min.js', function(data, textStatus, jqxhr) {
			if (typeof onComplete !== "function") {
				onComplete = function(section){};
			}
			var tags = window.wikiTags;
			tags.push(window.genreCategory);
			tags = tags.join(',');
			/**
			 *	Start Netbar Implamentation
			 */
			if(config.showInNetbar){
				$('.live-on-twitch').show();
				// Init bunnyears
				var netbarBunnyEars = new BunnyEars({
					'id': 'be-netbar',
					'tags': tags,
					'host': config.host,
					'streamCount': 10,
					'beforeView': "<!-- Start View -->",
					'afterView': "<!-- End View -->",
					'viewTemplate': "%stream%%name%%status%%viewers%",
					'showViewMore': true,
					'forceTagPrefix': true,
					'debug': config.showDebug,
					'gaTrackingId' : config.gaTrackingId
				}).overrideStationsURL(config.stationsServiceURL);

				netbarBunnyEars.loadStreams(function(){
					if (!netbarBunnyEars.streams) {
						// stations failed. Bail out!
						$(".live-on-twitch").remove();
					}
				});

				var timeoutHolder;
				var timeoutInterval = 100;

				var netbarHover = function() {
						// append wrapper to end of body
						if( ! $("#lotBeWrapper").length ) {
							$('body').append("<div id=\"lotBeWrapper\"></div>");
							netbarBunnyEars.tuneIn("#lotBeWrapper",{},function(){
								$('#lotBeWrapper .beFlexItem').last().addClass('beFlexItemMore');
								$('#lotBeWrapper a').append("<div class=\"beSeeMoreStreams\">See More Streams</div>");
								$('#lotBeWrapper').show();
								onComplete('netbar');
							});
						}

						$('.lot-nb-button').addClass('lot-nb-button-hover');
						$('#lotBeWrapper').show();

						// handle hiding and showing elements so we keep the show more view.
						var firstOffset = null;
						var hidden = 0;
						// Drop the class for all as too not break logic.
						$('#lotBeWrapper .beFlexItem')
							.show()
							.removeClass('beFlexShowing')
							.removeClass('beFlexHidden');

						$('#lotBeWrapper .beFlexItem').each(function() {
							var offsetTop = $(this).offset().top;

							if (!firstOffset) {
								firstOffset = offsetTop;
							}

							if (! $(this).hasClass('beFlexItemMore')) { // leave the last one alone.

								if (offsetTop == firstOffset) {
									$(this)
										.addClass('beFlexShowing')
										.removeClass('beFlexHidden');
								} else {
									hidden++;
									$(this)
										.removeClass('beFlexShowing')
										.addClass('beFlexHidden');
								}
							}
						});

						// hide last showing so we can load up get more
						if(hidden){
							$('#lotBeWrapper .beFlexShowing').last()
							.removeClass('beFlexShowing')
							.addClass('beFlexHidden');
						} else {
							// not hidden, so lets see if the beFlexItemMore is hidden
							if ($('#lotBeWrapper .beFlexItemMore').offset().top !== firstOffset) {
								// we have hit the "all the streams are showing but no show more edge case"
								$('#lotBeWrapper .beFlexShowing').last()
								.removeClass('beFlexShowing')
								.addClass('beFlexHidden');
							}
						}

						$('#lotBeWrapper .beFlexShowing').show();
						$('#lotBeWrapper .beFlexHidden').hide();


				}

				// Handle Hover
				$('body').on('mouseenter', '.live-on-twitch', function(){
					timeoutHolder = setTimeout(function(){
						netbarHover();
					},timeoutInterval);
				});

				$('body').on('mouseenter', '#lotBeWrapper', function(){
					netbarHover();
				});

				$('body').on('mouseleave', '.live-on-twitch, #lotBeWrapper', function() {
					if (!$('#lotBeWrapper').is(":visible")) { clearTimeout(timeoutHolder); }
					$('.lot-nb-button').removeClass('lot-nb-button-hover');
					$('#lotBeWrapper').hide();
					$('#lotBeWrapper .beFlexItem')
						.show()
						.removeClass('beFlexShowing')
						.removeClass('beFlexHidden');
				});

			}

			/**
			 *	Start Siderail Implamentation
			 */
			if (config.showInSiderail && !config.userLoggedIn) {
				// prepend siderail header to siderail
				$('#twitchSiderail').prepend("<div class=\"siderail-liveontwitch\"></div>");

				// init bunnyears
				var siderailBunnyEars = new BunnyEars({
					'id': 'be-siderail',
					'tags': tags,
					'host': config.host,
					'streamCount': 3,
					'viewTemplate': "%stream%<div class=\"slot-details\">%name%%status%%viewers%</div>",
					'showViewMore': true,
					'customViewMore': 'See More Streams',
					'forceTagPrefix': true,
					'debug': config.showDebug,
					'gaTrackingId' : config.gaTrackingId
				}).overrideStationsURL(config.stationsServiceURL).tuneIn("#twitchSiderail", {}, function() {
					$('#twitchSiderail .beStreamerMoreView').html('See More Streams');
					$('#twitchSiderail .beSlot1 .beStreamerViewers').insertBefore('#twitchSiderail .beSlot1 .beStreamerName');
					$('#twitchSiderail').show();
					onComplete('siderail');
				});
			}
			/**
			 * Start TwitchNet Implamentation
			 */
			if (config.showAtBottom && !config.userLoggedIn) {
				var twitchNetBunnyEars = new BunnyEars({
					'id': 'be-twitchnet',
					'tags': tags,
					'host': config.host,
					'streamCount': 10,
					'viewTemplate': "%stream%%name%%status%%viewers%",
					'showViewMore': false,
					'forceTagPrefix': true,
					'debug': config.showDebug,
					'gaTrackingId' : config.gaTrackingId
				})
				.overrideStationsURL(config.stationsServiceURL)
				.tuneIn("#twitchNet", {}, function() {
					$("#twitchNet").prepend("<div class=\"twitchnet-liveontwitch\"></div>");
					$("#twitchNet").show();
					onComplete('bottom');
				});
			}
		});
	}
	$(function() {

		if (typeof mw.user.isAnon !== "undefined") {
			var userLoggedIn = !mw.user.isAnon();
		} else {
			var userLoggedIn = false;
		}

		var config = {
			'gaTrackingId'		: mw.config.get('wgBunnyEarsGaTrackingId'),
			'showDebug'			: mw.config.get('debug'),
			'stationsServiceURL': mw.config.get('wgStationsServiceURL'),
			'showInNetbar' 		: mw.config.get('wgBunnyEarsEnableNetbar'),
			'showInSiderail' 	: mw.config.get('wgBunnyEarsEnableSiderail'),
			'showAtBottom' 		: mw.config.get('wgBunnyEarsEnableTwitchNet'),
			'host'		   		: mw.config.get('wgServer').split("//")[1].trim(),
			'runExperiment'		: mw.config.get('wgBunnyEarsRunExperiment'),
			'userLoggedIn'		: userLoggedIn
		};

		handleBunnyEars(config);

		/*
			If experiment code below is brought back into play,
			remember to uncomment code in the CSS as well.
		 */
		/*
		if ( config.runExperiment ) {
			mw.log('BunnyEars Experiment Enabled.');
			if (config.showAtBottom && config.showInSiderail && $('#zergnetsiderail_container').length && $('#zergnetContainer').length) {
				// we can run the google experiment
				$.getScript('//www.google-analytics.com/cx/api.js?experiment=HP8VHG5mSgKCfcvYlZyaBA', function(data, textStatus, jqxhr) {
					var chosenVariation = (typeof(cxApi) === "undefined") ? Math.floor(Math.random() * 3) : cxApi.chooseVariation();
					if (chosenVariation == 1) {
						// Replace zergnet at bottom
						$('#zergnetsiderail_container').show();
						mw.log('Run variation: Replace zergnet at bottom');
						config.showInSiderail = false;
						var callback = function(section){
							if (section == "bottom") {
								$('#zergnetContainer').hide();
							}
						};
					}
					else if (chosenVariation == 2) {
						// replace zergnet in siderail
						$('#zergnetContainer').show();
						mw.log('Run variation: Replace zergnet in siderail');
						config.showAtBottom = false;
						var callback = function(section){
							if (section == "siderail") {
								$('#zergnetsiderail_container').hide();
							}
						};
					} else {
						// default is to not show any twitch stuff, so we will hide it during experience.
						mw.log('Run variation: Keep zergnet in place, NO twitch.');
						config.showInSiderail = false;
						config.showAtBottom = false;
						// show both zergnets.
						$('#zergnetContainer').show();
						$('#zergnetsiderail_container').show();
						var callback = function(){};
					}
					handleBunnyEars(config,callback);
				});
			} else {

				// show both zergnets.
				$('#zergnetContainer').show();
				$('#zergnetsiderail_container').show();

				mw.log('Unable to run experiment, disabling twitch placements completely.');
				mw.log('Please verify that both Siderail and Bottom placements are enabled, and the zergnet containers exist for them as well.');
				// We can't run experiments here.
				// Since we are in test-land right now, NO BUNNY EARS OUTSIDE OF NETBAR!!!!
				config.showInSiderail = false;
				config.showAtBottom = false;
				handleBunnyEars(config);

			}
		} else {
			// show both zergnets.
			$('#zergnetContainer').show();
			$('#zergnetsiderail_container').show();
			// Handle bunny ears settings exactly as passed.
			handleBunnyEars(config);
		}
		*/
	});
})(window, jQuery, mw);