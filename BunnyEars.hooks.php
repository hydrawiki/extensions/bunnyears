<?php
/**
 * Curse Inc.
 * BunnyEars
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		BunnyEars
 * @link		https://gitlab.com/hydrawiki
 *
**/

class BunnyEarsHooks {
	/**
	 * Add the Stations Service URL to javascript
	 * @param  array  $vars
	 * @return true
	 */
	public static function onResourceLoaderGetConfigVars( array &$vars ) {
		global $wgStationsServiceURL, $wgBunnyEarsEnableNetbar, $wgBunnyEarsEnableSiderail, $wgBunnyEarsEnableTwitchNet, $wgBunnyEarsGaTrackingId, $wgBunnyEarsRunExperiment;
		$vars['wgStationsServiceURL'] = $wgStationsServiceURL;
		$vars['wgBunnyEarsEnableNetbar'] = $wgBunnyEarsEnableNetbar;
		$vars['wgBunnyEarsEnableSiderail'] = $wgBunnyEarsEnableSiderail;
		$vars['wgBunnyEarsEnableTwitchNet'] = $wgBunnyEarsEnableTwitchNet;
		$vars['wgBunnyEarsGaTrackingId'] = $wgBunnyEarsGaTrackingId;
		$vars['wgBunnyEarsRunExperiment'] = $wgBunnyEarsRunExperiment;

		return true;
	}

	/**
	 * Handle special on extension registration bits.
	 *
	 * @access	public
	 * @return	void
	 */
	static public function onRegistration() {
		global $wgStationsServiceURL;

		// Make it set automatically... if its not already set!
		if (!isset($wgStationsServiceURL)) {
			if (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] == 'development') {
				$wgStationsServiceURL = "https://stations.cursetech.io";
			} else {
				$wgStationsServiceURL = "https://stations.cursetech.com";
			}
		}
	}

	/**
	 * Add resource loader modules globally.
	 *
	 * @param	OutputPage $out
	 * @param 	Skin       $skin
	 * @return	boolean	True
	 */
	static public function onBeforePageDisplay( OutputPage &$out, Skin &$skin ) {
		global $wgStationsServiceURL;
		$out->addModules( 'ext.bunnyears' );
		$out->addHeadItem("bunnyears style", '<link rel="stylesheet" type="text/css" href="'.$wgStationsServiceURL.'/be/bunnyears-1.1.min.css" />');
		return true;
	}

	/**
	 * Add Twitch slots into the side rail placement.
	 *
	 * @param 	array	$placements
	 * @return	boolean	True
	 */
	static public function onSideRailPlacements( &$placements ) {
		global $wgBunnyEarsEnableSiderail;
		if ($wgBunnyEarsEnableSiderail) {
			$placements['twitch-siderail'] = "<div id=\"twitchSiderail\"></div>";
		}
		return true;
	}

	/**
	 * Make sure a certain sort happens.
	 * @param  array $placements
	 * @return void
	 */
	static public function onSideRailPlacementsBeforeOutput( &$placements ) {
		$newPlacements = [];

		// make sure MREC is first.
		if (isset($placements['atfmrec'])) {
			$newPlacements['atfmrec'] = $placements['atfmrec'];
			unset($placements['atfmrec']);
		}

		// next zerg OR twitch
		if (isset($placements['zergnetsiderail_container'])) {
			$newPlacements['zergnetsiderail_container'] = $placements['zergnetsiderail_container'];
			unset($placements['zergnetsiderail_container']);
		}

		if (isset($placements['twitch-siderail'])) {
			$newPlacements['twitch-siderail'] = $placements['twitch-siderail'];
			unset($placements['twitch-siderail']);
		}

		// Make sure the upsell is next.
		if (isset($placements['gp-pro-upsell'])) {
			$newPlacements['gp-pro-upsell'] = $placements['gp-pro-upsell'];
			unset($placements['gp-pro-upsell']);
		}

		// Who cares, follow the order.
		if (count($placements)) {
			foreach ($placements as $n => $v) {
				$newPlacements[$n] = $v;
			}
		}

		$placements = $newPlacements;
		return true;
	}

	/**
	 * Add Twitch slots into the bottom placement.
	 *
	 * @param 	array	$placements
	 * @param	VectorTemplate $template
	 * @return	boolean	True
	 */
	static public function onBottomPlacements( &$placements, $template ) {
		global $wgBunnyEarsEnableTwitchNet;

		$skin = $template->getSkin();
		$showAds = HydraHooks::showAds($skin);
		$notMainPage = $skin->getTitle()->getText() != str_replace("_", " ", wfMessage('mainpage')->inContentLanguage()->text());

		if ($wgBunnyEarsEnableTwitchNet && $showAds && $notMainPage) {
			$placements['twitchNet'] = "<!-- twitch content will load here -->";
		}
		return true;
	}

	/**
	 * Add Twitchy Stuff
	 *
	 * @param 	array	$items
	 * @return	boolean	True
	 */
	static public function onNetbarLeftEnd( &$items ) {
		$items['live-on-twitch'] = "<span id=\"lotNbButton\" class=\"lot-nb-button\"></span>";
		return true;
	}
}
